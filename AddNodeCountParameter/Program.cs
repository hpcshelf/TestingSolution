﻿using System;
using System.IO;
using br.ufc.pargo.hpe.backend.DGAC.database;
using br.ufc.pargo.hpe.backend.DGAC.utils;

namespace AddNodeCountParameter
{
    class MainClass
    {
		static string BASE_PATH = Path.Combine(Constants.HPCShelf_HOME, "Case Study - Alite/workspace");

        public static void Main(string[] args)
        {
            string hpe_file_name;
            string module_data;
            ComponentType c;

            string path = "br.ufc.mdcc.backend.ec2.platform." + args[1] + ".EC2_" + args[2] + "_" + args[3];
            string file_name = "EC2_" + args[2] + "_" + args[3] + ".hpe";

            restoreBackup(path, file_name);

            readInput(path, file_name, out hpe_file_name, out module_data, out c);

            InnerComponentType new_ic = new InnerComponentType();
            new_ic.localRef = "node_count";
            new_ic.location = "br.ufc.mdcc.hpcshelf.quantifier.IntUp/IntUp.hpe";
            new_ic.multiple = false;
            new_ic.name = "IntUp";
            new_ic.package = "br.ufc.mdcc.hpcshelf.quantifier";
            new_ic.parameter_id = "node_count";
            new_ic.quantifier_value = "1";
            new_ic.exposed = false;

            ParameterType new_par = new ParameterType();
            new_par.componentRef = "node_count";
            new_par.formFieldId = "node_count";
            new_par.order = 1;
            new_par.variance = VarianceType.contravariant;
            new_par.varName = "N";

            updateComponentInfo(c, new object[] { new_par, new_ic });
            updateFile(c, "user", Path.Combine("br.ufc.mdcc.backend.ec2.platform.accelerated.EC2_F1_Large16x", "src", "1.0.0.0", "IEC2_F1_Large16x.cs"), args[0], args[1], args[2], args[3]);
            updateFile(c, "base",Path.Combine("br.ufc.mdcc.backend.ec2.platform.accelerated.EC2_F1_Large16x", "src", "1.0.0.0", "BaseIEC2_F1_Large16x.cs"), args[0], args[1], args[2], args[3]);

            writeOutput(hpe_file_name, module_data, c);

            path = "br.ufc.mdcc.backend.ec2.impl.platform." + args[1] + ".EC2_" + args[2] + "_" + args[3] + "_impl";
			file_name = "EC2_" + args[2] + "_" + args[3] + "_impl.hpe"; 

            restoreBackup(path, file_name);

			readInput(path, file_name, out hpe_file_name, out module_data, out c);

            ParameterSupplyType new_supply = new ParameterSupplyType();
            new_supply.cRef = "node_count";
            new_supply.direct = true;
            new_supply.varName = "N";

            updateComponentInfo(c, new object[] { new_supply, new_ic });
            updateFile(c, "user", Path.Combine("br.ufc.mdcc.backend.ec2.impl.platform.accelerated.EC2_F1_Large16x_impl", "src", "1.0.0.0", "IEC2_F1_Large16x_impl.cs"), args[0], args[1], args[2], args[3]);
            updateFile(c, "base", Path.Combine("br.ufc.mdcc.backend.ec2.impl.platform.accelerated.EC2_F1_Large16x_impl", "src", "1.0.0.0", "BaseIEC2_F1_Large16x_impl.cs"), args[0], args[1], args[2], args[3]);

            writeOutput(hpe_file_name, module_data, c);
        }

        private static void restoreBackup(string path, string file_name)
        {
            string file_name_backup = Path.Combine(BASE_PATH, path, file_name) + ".2.backup";

			if (File.Exists(file_name_backup))
			{
                File.Delete(Path.Combine(BASE_PATH, path, file_name));
                File.Copy(file_name_backup, Path.Combine(BASE_PATH, path, file_name));
				File.Delete(file_name_backup);
			}
		}

        private static void updateFile(ComponentType c, string srcType, string path_base, string family_1, string family_2, string type, string size)
        {
            string s0 = File.ReadAllText(Path.Combine(BASE_PATH, path_base));

            string path = path_base.Replace("Acceleration", family_1).Replace("accelerated", family_2).Replace("F1", type).Replace("Large16x", size);
            string s1 = s0.Replace("Acceleration", family_1).Replace("accelerated", family_2).Replace("F1", type).Replace("Large16x", size);

            File.WriteAllText(Path.Combine(BASE_PATH, path), s1);

			for (int k = 0; k < c.componentInfo.Length; k++)
			{
				object o = c.componentInfo[k];
				if (o is InterfaceType)
				{
					InterfaceType it = (InterfaceType)o;

                    SourceType s = it.sources[0];

                    foreach (SourceFileType f in s.file)
					{
                        if (f.srcType.Equals(srcType))
						{
                            f.contents = s1;
							break;
						}
					}
				}
			}
		}

        private static void updateComponentInfo(ComponentType c, object[] v)
        {
            object[] new_componentInfo = new object[c.componentInfo.Length + v.Length];
            for (int i = 0; i < v.Length; i++)
			    new_componentInfo[i] = v[i];
            for (int i = v.Length; i < new_componentInfo.Length; i++)
                new_componentInfo[i] = c.componentInfo[i - v.Length];
            c.componentInfo = new_componentInfo;
		}

        private static void readInput(string name_1, string name_2, out string hpe_file_name, out string module_data, out ComponentType c)
        {
			hpe_file_name = Path.Combine(BASE_PATH, name_1, name_2);
			module_data = File.ReadAllText(hpe_file_name);
			c = LoaderApp.deserialize<ComponentType>(module_data);
		}

        private static void writeOutput(string hpe_file_name, string module_data, ComponentType c)
        {
			if (!File.Exists(hpe_file_name + ".2.backup"))
				File.WriteAllText(hpe_file_name + ".2.backup", module_data);

			string module_data_out = LoaderApp.serialize<ComponentType>(c);

			File.WriteAllText(hpe_file_name, module_data_out);
		}
    }
}
