﻿﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting;
using br.ufc.mdcc.hpcshelf.core;
using br.ufc.pargo.hpe.backend.DGAC.utils;
using br.ufc.pargo.hpe.basic;

namespace PopulateCatalog
{
    class MainClass
    {
        private static Assembly MyResolveEventHandler(object sender, ResolveEventArgs args)
        {
            int index;
            string class_name = (index = args.Name.IndexOf(',')) > 0 ? args.Name.Substring(0, index) : args.Name;

            string assembly_file = Path.Combine(Constants.PATH_GAC, class_name.Substring(0, class_name.LastIndexOf('.')), class_name + ".dll");

            //Load the assembly from the specified path.                    
            Assembly MyAssembly = File.Exists(assembly_file) ? Assembly.LoadFrom(assembly_file) : null;

            //Return the loaded assembly.
            return MyAssembly;
        }

        private static void Main(string[] args)
        {
			decimal a1 = int.MaxValue;
			decimal a2 = int.MinValue;
			
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.AssemblyResolve += new ResolveEventHandler(MyResolveEventHandler);

            string session_id;
            CoreServices core_services = new CoreServices("populate-112", out session_id);

            int jump = 1;

            foreach (string arg in args)
            {
                string path_catalog_folder = Path.Combine(Constants.HPCShelf_HOME, arg);
                string path_deploy_file = Path.Combine(path_catalog_folder, "to_deploy.txt");
                string[] contents = File.ReadAllLines(path_deploy_file);

                //foreach (string line in contents)
                for (int i=jump-1; i<contents.Length; i++)
                {
                    string line = contents[i];

                    Console.Write(line);

                    bool fail = true;
                    while (fail)
                    {
                        if (line.Equals("STOP"))
                            break;
                        try
                        {
                            if (!line.StartsWith("#"))
                            {
                                string file_name = Path.Combine(path_catalog_folder, line);
                                file_name = Path.Combine(file_name, line.Substring(line.LastIndexOf('.') + 1) + ".hpe");

                                string snk_file_name = Path.Combine(path_catalog_folder, line);
                                snk_file_name = Path.Combine(snk_file_name, line.Substring(line.LastIndexOf('.') + 1) + ".snk");

                                string hpe_contents = File.ReadAllText(file_name);
                                byte[] bs = File.ReadAllBytes(snk_file_name);

                                Stopwatch sw = new Stopwatch();
                                sw.Start();
                                core_services.register(hpe_contents, bs);
                                sw.Stop();
                                Console.WriteLine("   *****    Elapsed={0}", sw.Elapsed);

                            }
                            fail = false;
                        }
                        catch (Exception e)
                        {
                            fail = true;
                        }
                    }
                }
            }


            Console.WriteLine("END CATALOG ...");
        }
    }
}
