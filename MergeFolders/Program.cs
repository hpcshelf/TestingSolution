﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MergeFolders
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            string path = "/home/heron/Dropbox/Copy/ufc_mdcc_hpc/HPC-Shelf/Case Study - Alite/workspace/locales";

            string[] folders = Directory.GetDirectories(path);
            foreach (string source_f in folders)
            {
                string fname = Path.GetFileName(source_f);

                File.WriteAllText(Path.Combine(source_f, ".project"), File.ReadAllText(Path.Combine(source_f,".project")).Replace("br.ufc.mdcc.hpcshelf.quantifier.IntUp", fname));

            }




            //string path_source = "/home/heron/hpe/heron/cache";
			//string path_target = "/home/heron/Dropbox/Copy/ufc_mdcc_hpc/HPC-Shelf/Case Study - Alite/workspace";

            //CopyAll(path_source, path_target);

			/*string[] folders = Directory.GetDirectories(path_source);
            foreach (string source_f in folders)
            {
                string fname = Path.GetFileName(f);

            }*/




       /*     IDictionary<string, IList<string>> l = new Dictionary<string, IList<string>>();
            foreach (string f in folders)
            {
                IList<string> e;
                if (!l.TryGetValue(f.ToLower(), out e))
                    l[f.ToLower()] = new List<string>();
                l[f.ToLower()].Add(f);
			}

            foreach (KeyValuePair<string,IList<string>> kv in l)
            {
                if (kv.Value.Count == 2)
                {
                    string f1 = kv.Value[0];
                    string f2 = kv.Value[1];

                    string upper = !f1.ToLower().Equals(f1) ? f1 : f2;
					string lower = !f1.ToLower().Equals(f1) ? f2 : f1;

                    CopyAll(lower, upper);

                }
            }
*/

        }

		public static void CopyAll(string source, string target)
		{
            // Copy each file into it's new directory.
            foreach (string fi in Directory.GetFiles(source))
            {
                string fi_ = Path.GetFileName(fi);

                Console.WriteLine(@"Moving {0}\{1}", target, fi_);
                File.Copy(fi, Path.Combine(target, fi_), true);
            }

            // Copy each subdirectory using recursion.
            foreach (string diSourceSubDir in Directory.GetDirectories(source))
            {
                string diSourceSubDir_ = Path.GetFileName(diSourceSubDir);

                if (!Directory.Exists(Path.Combine(target, diSourceSubDir_)))
                    Directory.CreateDirectory(Path.Combine(target, diSourceSubDir_));
                CopyAll(diSourceSubDir, Path.Combine(target, diSourceSubDir_));
                //if (Directory.GetFiles(diSourceSubDir).Length == 0)
                //    Directory.Delete(diSourceSubDir);
            }
		}
    }
}
