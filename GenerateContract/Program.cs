﻿﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
//using br.ufc.pargo.hpe.backend.DGAC.utils;
using ContextualContracts;

namespace GenerateContract
{

    class MainClass
    {
        private static string home_path = null;

        public static string HOME_PATH
        {
            get
            {
                if (home_path == null)
                    home_path = (Environment.OSVersion.Platform == PlatformID.Unix || Environment.OSVersion.Platform == PlatformID.MacOSX)
                                      ? Environment.GetEnvironmentVariable("HOME") : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");
                return home_path;
            }
        }

        public static string HPCShelf_HOME
        {
            get
            {
                string hpcshelf_home = Environment.GetEnvironmentVariable("HPCShelf_HOME");
                 Console.WriteLine("HPCShelf_HOME is null ? " + (hpcshelf_home == null ? "null" : hpcshelf_home));
                return hpcshelf_home;
            }
        }

        static string CATALOG_PATH = Path.Combine(HPCShelf_HOME, "Case Study - Alite/workspace");
        static string OUTPUT_PATH = Path.Combine(HPCShelf_HOME, "Case Study - Alite/signatures");

        private static string framework_path = null;

        public static void Main(string[] args)
        {
            // make_covariant();
            // generate_context_parameters_latex("br.ufc.mdcc.hpcshelf.platform.Platform");


            // 1. GERAÇÂO DOS CONTRATOS E ASSINATURAS DE PLATAFORMA (.contract) A PARTIR DA PLANILHA

             
            framework_path = "ec2_framework";

            read_ec2_contracts_from_csv_2(Path.Combine(OUTPUT_PATH,"contracts.csv"), new_ec2_contract);
            expand_ec2_contracts_by_locales(Path.Combine(OUTPUT_PATH,"locales.csv"));

            read_ec2_contracts_from_csv_2(Path.Combine(OUTPUT_PATH, "contracts.csv"), new_ec2_abstractcomponent);
            expand_ec2_concretecomponents_by_locales(Path.Combine(OUTPUT_PATH, "locales.csv"));


            // -----------------------------------------------------------------------------------------------------------------------
            // 2. GERAÇÃO DOS COMPONENTES BLAS A PARTIR DE COMPONENTES "BASE" (do workspace) !!! 


            IDictionary<string, string[]> m = new Dictionary<string, string[]>
            {
                {"diag", new string[] {"br.ufc.mdcc.hpcshelf.blas.configure.diag.N","br.ufc.mdcc.hpcshelf.blas.configure.diag.U"} },
                {"uplo", new string[] {"br.ufc.mdcc.hpcshelf.blas.configure.uplo.L","br.ufc.mdcc.hpcshelf.blas.configure.uplo.U" } },
                {"trans_A", new string[] {"br.ufc.mdcc.hpcshelf.blas.configure.trans.C","br.ufc.mdcc.hpcshelf.blas.configure.trans.N","br.ufc.mdcc.hpcshelf.blas.configure.trans.T" } },
                {"trans_B", new string[] {"br.ufc.mdcc.hpcshelf.blas.configure.trans.C", "br.ufc.mdcc.hpcshelf.blas.configure.trans.N", "br.ufc.mdcc.hpcshelf.blas.configure.trans.T" } },
                {"trans", new string[] {"br.ufc.mdcc.hpcshelf.blas.configure.trans.C", "br.ufc.mdcc.hpcshelf.blas.configure.trans.N", "br.ufc.mdcc.hpcshelf.blas.configure.trans.T" } },
                {"side", new string[] {"br.ufc.mdcc.hpcshelf.blas.configure.side.L","br.ufc.mdcc.hpcshelf.blas.configure.side.R" } },
                {"data_type", new string[] {"br.ufc.mdcc.common.DFloat","br.ufc.mdcc.common.DComplex","br.ufc.mdcc.common.Integer","br.ufc.mdcc.common.SFloat","br.ufc.mdcc.common.SComplex" } },
                {"platform-node-accelerator-architecture", new string[] {"br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Turing","br.ufc.mdcc.hpcshelf.platform.node.accelerator.architecture.Kepler","br.ufc.mdcc.hpcshelf.platform.node.accelerator.Architecture" } }
            };


           // adjust_blas_abstract_components();
            create_blas_base_concrete_components();
            expand_computation_contracts(m, Path.Combine(OUTPUT_PATH, "computations.csv"));


            // -----------------------------------------------------------------------------------------------------------------------

            // 3. GERAÇÂO DOS CONTRATOS E ASSINATURAS BLAS A PARTIR DOS COMPONENTES GERADOS EM (2)

            framework_path = "blas_framework";

            foreach (string path in cs2)
            {
                string file_name = path.Substring(path.LastIndexOf(".") + 1);

                string hpe_file_name = Path.Combine(CATALOG_PATH, path, file_name + ".hpe");
                string output_file_name = Path.Combine(OUTPUT_PATH, "blas_contracts", path + ".contract");

                mk_contract(hpe_file_name, output_file_name);
            }
        }

        private static void generate_context_parameters_latex(string path_abstract)
        {
			string file_name_abstract = path_abstract.Substring(path_abstract.LastIndexOf(".") + 1);
			string package_abstract = path_abstract.Substring(0, path_abstract.LastIndexOf("."));
			string hpe_file_name_abstract = Path.Combine(CATALOG_PATH, path_abstract, file_name_abstract + ".hpe");

            IDictionary<string, ParameterType> w;
            IDictionary<string, InnerComponentType> v;
            give_parameters_and_supplies(hpe_file_name_abstract, out _, out w, out _, out v);

            foreach (ParameterType p in w.Values)
            {
                InnerComponentType c = v[p.componentRef];

                Console.WriteLine("\\textbf{\\textit{" + p.formFieldId +"}} :> " + c.package + ".\\textsc{" + c.name + "} \\\\");
            }

		}

        private static void make_covariant()
        {
            string[] dirs = Directory.GetDirectories(CATALOG_PATH);
            foreach (string dir_path in dirs)
            {
                string dir_name = Path.GetFileName(dir_path);

                if (dir_name.StartsWith("br.ufc.mdcc.backend.ec2") || dir_name.StartsWith("br.ufc.mdcc.hpcshelf.platform"))
                {
                    string hpe_file_name = Path.Combine(dir_path, dir_name.Substring(dir_name.LastIndexOf('.') + 1) + ".hpe");
                    ComponentType c = deserialize<ComponentType>(File.ReadAllText(hpe_file_name));
                    foreach (object o in c.componentInfo)
                    {
                        if (o is ParameterType)
                            ((ParameterType)o).variance = VarianceType.covariant;
                    }
                    Console.WriteLine(dir_name);
                    File.WriteAllText(hpe_file_name, serialize<ComponentType>(c));
                }
            }

        }

        private static void create_blas_base_concrete_components()
        {
			foreach (string path_abstract in cs1)
            {
                string path_concrete = path_abstract.Replace("blas.level", "blas.impl.level");

                string file_name_abstract = path_abstract.Substring(path_abstract.LastIndexOf(".") + 1);
                string package_abstract = path_abstract.Substring(0, path_abstract.LastIndexOf("."));
                string hpe_file_name_abstract = Path.Combine(CATALOG_PATH, path_abstract, file_name_abstract + ".hpe");

				string file_name_concrete = path_concrete.Substring(path_concrete.LastIndexOf(".") + 1);
				string package_concrete = path_concrete.Substring(0, path_concrete.LastIndexOf("."));
				string hpe_file_name_concrete = Path.Combine(CATALOG_PATH, path_concrete, file_name_concrete + ".hpe");

				if (!Directory.Exists(Path.Combine(CATALOG_PATH, path_concrete)))
				{
					Directory.CreateDirectory(Path.Combine(CATALOG_PATH, path_concrete));
					File.WriteAllText(Path.Combine(CATALOG_PATH, path_concrete, ".project"), File.ReadAllText(Path.Combine(CATALOG_PATH, path_abstract, ".project")).Replace(path_abstract, path_concrete));
				}

				ComponentType c = deserialize<ComponentType>(File.ReadAllText(hpe_file_name_abstract));

                c.header.isAbstract = false;
                c.header.isAbstractSpecified = true;
                c.header.name = file_name_concrete;
                c.header.packagePath = package_concrete;
                c.header.baseType.extensionType.Item = true;
                c.header.baseType.extensionType.ItemElementName = ItemChoiceType.implements;
                c.header.baseType.component.name = file_name_abstract;
                c.header.baseType.component.package = package_abstract;
                c.header.baseType.component.location = Path.Combine(path_abstract + "." + file_name_abstract, file_name_abstract + ".hpe");
                c.header.baseType.component.hash_component_UID = null;
                //c.header.baseType.component.parameter = null;
                c.header.baseType.component.unitBounds = null;

                List<ParameterRenaming> renaming_list = new List<ParameterRenaming>();

                IList<object> o_list = new List<object>();
                IDictionary<string, InnerComponentType> ics = new Dictionary<string, InnerComponentType>();
                IList<string> sids = new List<string>();
                foreach (object o in c.componentInfo)
                {
                    object o_ = o;

                    if (!(o is InnerComponentType))
                    {
                        if (o is ParameterType)
                        {
                            ParameterType p = (ParameterType)o;
                            ParameterSupplyType s = new ParameterSupplyType();
                            s.cRef = p.componentRef;
                            s.direct = true;
                            s.varName = p.varName;
                            sids.Add(s.cRef);
                            o_ = s;

                            ParameterRenaming r = new ParameterRenaming();
                            r.formFieldId = p.formFieldId;
                            r.varName = p.varName;
                            r.order = p.order;
                            renaming_list.Add(r);
                        }
                        else if (o is InterfaceType)
                        {
                            InterfaceType it = (InterfaceType)o;
                            it.iRef = it.iRef + "Impl";
                        } 
                        else if (o is UnitType)
                        {
                            UnitType ut = (UnitType)o;
                            ut.iRef = ut.iRef + "Impl";
                        }

                        if (!(o is ParameterSupplyType))
                            o_list.Add(o_);
                    }
                    else
                    {
                        InnerComponentType ic = (InnerComponentType)o;
                        ics[ic.localRef] = ic;
                    }
                }

				renaming_list.Sort(delegate (ParameterRenaming x, ParameterRenaming y) { return x.order - y.order; });
				c.header.baseType.component.parameter = new ParameterRenaming[renaming_list.Count];
				renaming_list.CopyTo(c.header.baseType.component.parameter, 0);

				foreach (string sid in sids)
                    o_list.Add(ics[sid]);

                c.componentInfo = new object[o_list.Count];
                o_list.CopyTo(c.componentInfo, 0);

				File.WriteAllText(hpe_file_name_concrete, serialize<ComponentType>(c));
			}
		}

        private static void adjust_blas_abstract_components()
        {

            IDictionary<string, int> other_parameters_order = new Dictionary<string, int>() { { "data_type", 0 }, { "distribution", 1 }, { "matrix_type", 2 }, { "uplo", 3 }, { "diag", 4 }, { "side", 5 }, { "trans_A", 6 }, { "trans_B", 7 }, { "trans", 8 } };

            foreach (string path in cs1)
            {
                string file_name = path.Substring(path.LastIndexOf(".") + 1);
                string hpe_file_name = Path.Combine(CATALOG_PATH, path, file_name + ".hpe");

                Console.WriteLine(path);

                if (File.Exists(hpe_file_name + ".1"))
                    File.Delete(hpe_file_name + ".1");

                File.Copy(hpe_file_name, hpe_file_name + ".1");

                ComponentType c /*= deserialize<ComponentType>(File.ReadAllText(hpe_file_name))*/;
                IDictionary<string, ParameterType> all_parameter;
                IDictionary<string, InnerComponentType> all_parameter_components;
                give_parameters_and_supplies(hpe_file_name, out c, out all_parameter, out _, out all_parameter_components);

                ComponentType c_super;
                IDictionary<string, ParameterType> all_super_parameter;
                IDictionary<string, InnerComponentType> all_super_parameter_components;
                give_parameters_and_supplies(Path.Combine(CATALOG_PATH, c.header.baseType.component.location), out c_super, out all_super_parameter, out _, out all_super_parameter_components);

                List<ParameterRenaming> pr_list = new List<ParameterRenaming>();

                foreach (ParameterType p in all_super_parameter.Values)
                {
                    ParameterRenaming pr = new ParameterRenaming();
                    pr.formFieldId = p.formFieldId;
                    pr.varName = p.varName;

                    pr_list.Add(pr);
                }

                c.header.baseType.component.parameter = new ParameterRenaming[pr_list.Count];
                pr_list.CopyTo(c.header.baseType.component.parameter, 0);

                int counter = 0;
                int proper_parameter_count = 0;
                IList<string> varToRename = new List<string>();
                IList<string> to_remove = new List<string>();
                foreach (KeyValuePair<string, ParameterType> kv in all_parameter)
                    if (!all_super_parameter.ContainsKey(kv.Key))
                    {
                        if (kv.Key.StartsWith("platform-"))
                            to_remove.Add(kv.Key);
                        else
                            proper_parameter_count++;
                        all_parameter[kv.Key].order = counter++;
                    }
                    else if (!kv.Value.varName.Equals(all_super_parameter[kv.Key].varName))
                        varToRename.Add(kv.Key);

                foreach (string par_id in varToRename)
                    all_parameter[par_id].varName = all_super_parameter[par_id].varName;

                foreach (KeyValuePair<string, ParameterType> kv in all_super_parameter)
                {
                    // só acrescentar se tiver um novo parâmetro de plataforma.
                    if (kv.Key.StartsWith("platform-") && !all_parameter.ContainsKey(kv.Key))
                    {
                        all_parameter[kv.Key] = all_super_parameter[kv.Key];
                        all_parameter_components[kv.Key] = all_super_parameter_components[kv.Key];
                    }

                    if (all_parameter.ContainsKey(kv.Key))
                        all_parameter[kv.Key].order = all_super_parameter[kv.Key].order + proper_parameter_count;
                }

                foreach (string r in to_remove)
                {
                    all_parameter.Remove(r);
                    all_parameter_components.Remove(r);
                }

                IList<object> o_list_2 = new List<object>();

                foreach (ParameterType o in all_parameter.Values) { o_list_2.Add(o); }
                foreach (InnerComponentType o in all_parameter_components.Values)
                {
                    if (o.localRef.StartsWith("platform"))
                    {
                        InnerComponentType o_super = all_super_parameter_components[o.localRef];
                        o.parameter = o_super.parameter;
                    }

                    o_list_2.Add(o);
                }

                foreach (object o in c.componentInfo)
                    if (!(o is ParameterType || o is InnerComponentType))
                        o_list_2.Add(o);

				c.componentInfo = new object[o_list_2.Count];
                o_list_2.CopyTo(c.componentInfo, 0);

                File.WriteAllText(hpe_file_name, serialize<ComponentType>(c));
            }
        }

	/*	private static void adjust_blas_abstract_components_2()
		{
            foreach (string path in cs1)
			{
				string file_name = path.Substring(path.LastIndexOf(".") + 1);
				string hpe_file_name = Path.Combine(CATALOG_PATH, path, file_name + ".hpe");

				Console.WriteLine(path);

				ComponentType c = deserialize<ComponentType>(File.ReadAllText(hpe_file_name));

                ParameterType acc_architecture = null;
                ParameterType acc_model = null;

                foreach (object o in c.componentInfo)
                {
                    if (o is ParameterType)
                    {
                        ParameterType p = (ParameterType)o;
                        if (p.formFieldId.Equals("platform-node-accelerator-architecture"))
                            acc_architecture = p;
                        else if (p.formFieldId.Equals("platform-node-accelerator-model"))
                            acc_model = p;
                    }
                }

                acc_architecture.order--;

	
				File.WriteAllText(hpe_file_name, serialize<ComponentType>(c));
			}
		}*/
		
        private static void expand_computation_contracts(IDictionary<string, string[]> m, string file_name)
        {
            string[] lines = File.ReadAllLines(file_name);
			string[] yyy = { "", "diag", "uplo", "trans_A", "trans_B", "trans", "side", "data_type", "platform-node-accelerator-architecture" };

            for (int i = 1; i < lines.Length; i++)
            {
                string[] line = lines[i].Split(',');

                IList<string> columns = new List<string>();
                IList<IList<string>> l0 = new List<IList<string>>();

                for (int y = 1; y < yyy.Length; y++)
                {
                    IList<IList<string>> l1 = new List<IList<string>>();

                    if (line[y].Equals("s"))
                    {
                        columns.Add(yyy[y]);
                        if (l0.Count > 0)
                            foreach (IList<string> ll in l0)
                            {
                                foreach (string s in m[yyy[y]])
                                {
                                    IList<string> new_ll = new List<string>(ll);
                                    new_ll.Add(s);
                                    l1.Add(new_ll);
                                }
                            }
                        else
                        {
                            foreach (string s in m[yyy[y]])
                            {
                                IList<string> l = new List<string>();
                                l.Add(s);
                                l1.Add(l);
                            }
                        }
                        l0 = l1;
                    }
                }

                ComponentType c;
                IDictionary<string, ParameterSupplyType> w;
                IDictionary<string, InnerComponentType> v;

                string component_ref = line[0];
                string path_name = Path.Combine(CATALOG_PATH, component_ref);
                string hpe_file_name = component_ref.Substring(component_ref.LastIndexOf('.') + 1);
                string hpe_file_name_fullpath = Path.Combine(path_name, hpe_file_name + ".hpe");

                give_parameters_and_supplies(hpe_file_name_fullpath, out c, out _, out w, out v);

                InterfaceType it = null;
                UnitType ut = null;

                string file_0 = null;
                string file_1 = null;

                foreach (object o in c.componentInfo)
                {
                    if (o is InterfaceType)
                    {
                        it = (InterfaceType)o;
                        file_0 = it.sources[0].file[0].contents;
                        file_1 = it.sources[0].file[1].contents;
                    }
                    else if (o is UnitType)
                    {
                        ut = (UnitType)o;
                    }
                }


                foreach (IList<string> argument_list in l0)
                {
                    string suffix = "_";
                    for (int k = 0; k < columns.Count; k++)
                    {
                        InnerComponentType e = v[w[columns[k]].cRef];
                        string p = argument_list[k];

                        e.package = p.Substring(0, p.LastIndexOf('.'));
                        e.name = p.Substring(p.LastIndexOf('.') + 1);
                        e.location = Path.Combine(e.package + "." + e.name, e.name + ".hpe");

                        suffix += e.name + "_";
                    }

                    c.header.name = hpe_file_name + suffix;


                    it.iRef = ut.iRef = "I" + hpe_file_name + suffix + "Impl";
                    it.sources[0].moduleName = component_ref + "." + it.iRef;

                    it.sources[0].file[0].contents = file_0.Replace("impl.level1." + hpe_file_name, "impl.level1." + hpe_file_name + suffix).Replace("class BaseI" + hpe_file_name, "class BaseI" + hpe_file_name + suffix);
                    it.sources[0].file[0].name = "Base" + it.iRef + ".cs";


                    it.sources[0].file[1].contents = file_1.Replace("impl.level1." + hpe_file_name, "impl.level1." + hpe_file_name + suffix).Replace("class I" + hpe_file_name, "class I" + hpe_file_name + suffix).Replace(": BaseI" + hpe_file_name, ": BaseI" + hpe_file_name + suffix);
                    it.sources[0].file[1].name = it.iRef + ".cs";

                    string new_path_name = Path.Combine(CATALOG_PATH, component_ref + suffix);
                    string new_hpe_file_name = hpe_file_name + suffix;
                    string new_hpe_file_name_fullpath = Path.Combine(new_path_name, new_hpe_file_name + ".hpe");

                    if (!Directory.Exists(new_path_name))
                    {
                        DirectoryCopy(path_name, new_path_name, true);
                        File.Delete(Path.Combine(new_path_name, hpe_file_name + ".snk"));
                        File.Delete(Path.Combine(new_path_name, hpe_file_name + ".pub"));
                    }

                    if (!Directory.Exists(Path.Combine(new_path_name, "src")))
                    {
                        Directory.CreateDirectory(Path.Combine(new_path_name, "src"));
						Directory.CreateDirectory(Path.Combine(new_path_name, "src", "1.0.0.0"));
					}

                    foreach (string delete_src_file in Directory.GetFiles(Path.Combine(new_path_name, "src", "1.0.0.0")))
                        File.Delete(delete_src_file);

					File.WriteAllText(Path.Combine(new_path_name, "src", "1.0.0.0", it.sources[0].file[0].name), it.sources[0].file[0].contents);
                    File.WriteAllText(Path.Combine(new_path_name, "src", "1.0.0.0", it.sources[0].file[1].name), it.sources[0].file[1].contents);

                    File.Delete(Path.Combine(new_path_name, hpe_file_name + ".hpe"));
                    File.WriteAllText(Path.Combine(new_path_name, ".project"), File.ReadAllText(Path.Combine(new_path_name, ".project")).Replace(hpe_file_name, new_hpe_file_name));

                    string contents = serialize<ComponentType>(c);
                    File.WriteAllText(new_hpe_file_name_fullpath, contents);
                }
            }
        }

        private static void give_parameters_and_supplies(string hpe_file_name_fullpath, out ComponentType c, out IDictionary<string, ParameterType> w1, out IDictionary<string, ParameterSupplyType> w2, out IDictionary<string, InnerComponentType> v)
        {
			w1 = new Dictionary<string, ParameterType>();
			w2 = new Dictionary<string, ParameterSupplyType>();
			v = new Dictionary<string, InnerComponentType>();

            c = deserialize<ComponentType>(File.ReadAllText(hpe_file_name_fullpath));

			foreach (object o in c.componentInfo)
			{
				if (o is ParameterType)
				{
					ParameterType e = (ParameterType)o;
                    w1[e.formFieldId] = e;
				}
				else if (o is ParameterSupplyType)
				{
					ParameterSupplyType e = (ParameterSupplyType)o;
					w2[e.cRef] = e;
				}
				else if (o is InnerComponentType)
				{
					InnerComponentType e = (InnerComponentType)o;
					v[e.localRef] = e;
				}
			}
		}

        private static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
		{
			// Get the subdirectories for the specified directory.
			DirectoryInfo dir = new DirectoryInfo(sourceDirName);

			if (!dir.Exists)
			{
				throw new DirectoryNotFoundException(
					"Source directory does not exist or could not be found: "
					+ sourceDirName);
			}

			DirectoryInfo[] dirs = dir.GetDirectories();
			// If the destination directory doesn't exist, create it.
			if (!Directory.Exists(destDirName))
			{
				Directory.CreateDirectory(destDirName);
			}

			// Get the files in the directory and copy them to the new location.
			FileInfo[] files = dir.GetFiles();
			foreach (FileInfo file in files)
			{
				string temppath = Path.Combine(destDirName, file.Name);
				file.CopyTo(temppath, false);
			}

			// If copying subdirectories, copy them and their contents to new location.
			if (copySubDirs)
			{
				foreach (DirectoryInfo subdir in dirs)
				{
					string temppath = Path.Combine(destDirName, subdir.Name);
					DirectoryCopy(subdir.FullName, temppath, copySubDirs);
				}
			}
		}

        private static void expand_ec2_contracts_by_locales(string file_name)
        {
			string[] locales = File.ReadAllLines(file_name);

			string[] locale_name = locales[1].Split(',');
			string[] locale_cref = locales[2].Split(',');

            for (int i = 3; i < locales.Length; i ++)
            {
                string[] cost = locales[i].Split(',');

				string family = cost[0];
				string type = cost[1];
				string size = cost[2];

				string name_concrete = "br.ufc.mdcc.backend.ec2.impl.platform." + family + ".EC2_" + type + "_" + size;

                string contract_file_name = Path.Combine(OUTPUT_PATH, "ec2_base_contracts", name_concrete + ".contract");
                string contract_contents = File.ReadAllText(contract_file_name);

                ContextualContractType c = deserialize<ContextualContractType>(contract_contents);
                c.context_argument = new ContextArgumentType[2];
                c.context_argument[0] = new ContextArgumentType();
				c.context_argument[1] = new ContextArgumentType();

				for (int j = 3; j < locale_cref.Length; j++)
                {
                    if (decimal.TryParse(cost[j], out _))
                    {
                        c.context_argument[0].parameter_id = "node-cost_per_hour";
                        c.context_argument[0].argument = cost[j];
                        c.context_argument[0].typeSpecified = true;
                        c.context_argument[0].type = ContextArgumentTypeType.quantifier;

                        c.context_argument[1].parameter_id = "node-locale";
                        c.context_argument[1].argument = locale_cref[j];
                        c.context_argument[1].typeSpecified = true;
                        c.context_argument[1].type = ContextArgumentTypeType.component_name;

                        string new_name_concrete = "br.ufc.mdcc.backend.ec2.impl.platform." + family + ".EC2_" + type + "_" + size + "_" + locale_name[j];
                        string new_contract_contents = serialize<ContextualContractType>(c);
                        string new_contract_file_name = Path.Combine(OUTPUT_PATH, "ec2_virtualplatform_contracts", new_name_concrete + ".contract");

                        File.WriteAllText(new_contract_file_name, new_contract_contents);
                    }
                }
            }
		}


        private static void expand_ec2_concretecomponents_by_locales(string file_name)
		{
			string[] locales = File.ReadAllLines(file_name);

            string[] locale_id = locales[0].Split(',');
			string[] locale_name = locales[1].Split(',');
			string[] locale_cref = locales[2].Split(',');

            for (int i = 3; i < locales.Length; i++)
            {
                string[] cost = locales[i].Split(',');

                string family = cost[0];
                string type = cost[1];
                string size = cost[2];

                ComponentType c_abstract;
                ComponentType c_concrete;

                string component_ref_concrete = "br.ufc.mdcc.backend.ec2.impl.platform." + family + ".EC2_" + type + "_" + size + "_impl";
                string path_name_concrete = Path.Combine(CATALOG_PATH, component_ref_concrete);
                string hpe_file_name_concrete = component_ref_concrete.Substring(component_ref_concrete.LastIndexOf('.') + 1);
                string hpe_file_name_fullpath_concrete = Path.Combine(path_name_concrete, hpe_file_name_concrete + ".hpe");

                c_concrete = deserialize<ComponentType>(File.ReadAllText(hpe_file_name_fullpath_concrete));

                string component_ref_abstract = "br.ufc.mdcc.backend.ec2.platform." + family + ".EC2_" + type + "_" + size;
                string path_name_abstract = Path.Combine(CATALOG_PATH, component_ref_abstract);
                string hpe_file_name_abstract = component_ref_abstract.Substring(component_ref_abstract.LastIndexOf('.') + 1);
                string hpe_file_name_fullpath_abstract = Path.Combine(path_name_abstract, hpe_file_name_abstract + ".hpe");

                IDictionary<string, ParameterType> p;
                IDictionary<string, ParameterSupplyType> s;
                IDictionary<string, InnerComponentType> v;
                give_parameters_and_supplies(hpe_file_name_fullpath_abstract, out c_abstract, out p, out s, out v);

                List<ParameterRenaming> renaming_list = new List<ParameterRenaming>();
                foreach (ParameterType par in p.Values)
                {
                    ParameterRenaming renaming_item = new ParameterRenaming();
                    renaming_item.formFieldId = par.formFieldId;
                    renaming_item.order = par.order;
                    renaming_item.varName = par.varName;
                    renaming_list.Add(renaming_item);
                }

                renaming_list.Sort(delegate (ParameterRenaming x, ParameterRenaming y) { return x.order - y.order; });
                c_concrete.header.baseType.component.parameter = new ParameterRenaming[renaming_list.Count];
                renaming_list.CopyTo(c_concrete.header.baseType.component.parameter, 0);

                SourceFileType sft_platform_settigns = null;

                foreach (object oc in c_concrete.componentInfo)
                {
                    if (oc is InterfaceType)
                    {
                        InterfaceType oci = (InterfaceType)oc;

                        oci.sources = new SourceType[1];
                        oci.sources[0] = new SourceType();
                        oci.sources[0].deployType = oci.sources[0].sourceType = oci.sources[0].moduleName = "platform.settings";
                        oci.sources[0].file = new SourceFileType[1];
                        oci.sources[0].file[0] = new SourceFileType();
                        oci.sources[0].file[0].name = "platform.settings";
                        oci.sources[0].file[0].srcType = "platform.settings";
                        oci.sources[0].file[0].fileType = "platform.settings";
                        sft_platform_settigns = oci.sources[0].file[0]; 
                    }
                }


				for (int j = 3; j < locale_cref.Length; j++)
				{
                    if (decimal.TryParse(cost[j], out _) && !locale_cref[j].Equals("-"))
                    {
                        InnerComponentType e1 = v["node-locale"];
                        string pp = locale_cref[j];
						e1.package = pp.Substring(0, pp.LastIndexOf('.'));
						e1.name = pp.Substring(pp.LastIndexOf('.') + 1);
                        e1.location = Path.Combine(e1.package + "." + e1.name, e1.name + ".hpe");
                        e1.unitBounds = null;

                        adjust_parameter_renamings(e1);

						ParameterSupplyType s1 = new ParameterSupplyType();
                        s1.cRef = e1.localRef;
                        s1.varName = p[e1.localRef].varName;
						s1.direct = true;

						InnerComponentType e2 = v["node-cost_per_hour"];
                        e2.quantifier_value = cost[j];

                        ParameterSupplyType s2 = new ParameterSupplyType();
                        s2.cRef = e2.localRef;
                        s2.varName = p[e2.localRef].varName; ;
						s2.direct = true;

						//	InnerComponentType e3 = v["node-count"];

						//	ParameterSupplyType s3 = new ParameterSupplyType();
						//	s3.cRef = e3.localRef;
						//	s3.varName = p[e3.localRef].varName; ;
						//	s3.direct = true;

						//	InnerComponentType e4 = v["node-storage-size"];

						//	ParameterSupplyType s4 = new ParameterSupplyType();
						//	s4.cRef = e4.localRef;
						//	s4.varName = p[e4.localRef].varName; ;
						//	s4.direct = true;

						sft_platform_settigns.contents = contentsOfPlatformSettings(family, type, size, locale_id[j]);

						string new_component_ref = "br.ufc.mdcc.backend.ec2.impl.platform." + family + ".EC2_" + type + "_" + size + "_" + locale_name[j];
                        string new_path_name = Path.Combine(CATALOG_PATH, new_component_ref);
                        string new_hpe_file_name = new_component_ref.Substring(component_ref_concrete.LastIndexOf('.') + 1); ;
                        string new_hpe_file_name_fullpath = Path.Combine(new_path_name, new_hpe_file_name + ".hpe");

                        c_concrete.header.name = new_component_ref.Substring(new_component_ref.LastIndexOf('.') + 1);
                        c_concrete.header.hash_component_UID = null;
                        c_concrete.header.baseType.component.hash_component_UID = null;

                        IList<object> o_list = new List<object>();
                        foreach (object o in c_concrete.componentInfo)
                            if (! (o is ParameterType || o is InnerComponentType || o is ParameterSupplyType))
                                o_list.Add(o);

                        c_concrete.componentInfo = new object[8 + o_list.Count];
                        c_concrete.componentInfo[0] = e1;
                        c_concrete.componentInfo[1] = e2;
						//c_concrete.componentInfo[2] = e3;
						//c_concrete.componentInfo[3] = e4;
						c_concrete.componentInfo[4] = s1;
						c_concrete.componentInfo[5] = s2;
						//c_concrete.componentInfo[6] = s3;
						//c_concrete.componentInfo[7] = s4;
						for (int a = 8; a < o_list.Count + 8; a++)
                            c_concrete.componentInfo[a] = o_list[a - 8];

						if (!Directory.Exists(new_path_name))
                        {
                            DirectoryCopy(path_name_concrete, new_path_name, true);
                            File.Delete(Path.Combine(new_path_name, hpe_file_name_concrete + ".snk"));
                            File.Delete(Path.Combine(new_path_name, hpe_file_name_concrete + ".pub"));
                            File.Delete(Path.Combine(new_path_name, "src","1.0.0.0","I" + hpe_file_name_concrete + ".cs"));
							File.Delete(Path.Combine(new_path_name, "src", "1.0.0.0", "BaseI" + hpe_file_name_concrete + ".cs"));

                            File.Delete(Path.Combine(new_path_name, hpe_file_name_concrete + ".hpe.backup"));
                            File.Delete(Path.Combine(new_path_name, hpe_file_name_concrete + ".hpe.2.backup"));
                            File.Delete(Path.Combine(new_path_name, hpe_file_name_concrete + ".hpe.backup.2"));
						}

                        File.Delete(Path.Combine(new_path_name, hpe_file_name_concrete + ".hpe"));
                        File.WriteAllText(Path.Combine(new_path_name, ".project"), File.ReadAllText(Path.Combine(new_path_name, ".project")).Replace(hpe_file_name_concrete, new_hpe_file_name));
                        File.WriteAllText(Path.Combine(new_path_name, "src", "1.0.0.0","platform.settings"), sft_platform_settigns.contents);
						
                        string contents = serialize<ComponentType>(c_concrete);

                        contents = contents.Replace(hpe_file_name_concrete, new_hpe_file_name);

                        File.WriteAllText(new_hpe_file_name_fullpath, contents);
                    }
				}
			}
		}

        private static string contentsOfPlatformSettings(string family, string type, string size, string locale)
        {
            return "http://127.0.0.1:8077/BackendServices.asmx\n*;$node-count;" + locale + ";" + family + ";" + type + ";" + size;
        }

        private static void mk_signature(string path_in, string path_out)
        {
            if (File.Exists(path_out))
                return;

			ComponentType c;
			readInput(path_in, out c);
			AbstractComponentSignatureType signature = mk_signature(c);
			writeSignature(path_out, signature);
		}

        private static IDictionary<string, string> family_name = new Dictionary<string, string>()
                                                                {
																	{"general","GeneralPurpose"},
																	{"compute","Computation"},
																	{"accelerated", "Acceleration"},
																	{"memory","Memory"},
                                                                    {"storage","Storage"}
                                                                };

        private static void read_contract_from_csv(string file_name)
        {
            string[] contracts = File.ReadAllLines(file_name);
            int columns_size = contracts[0].Split(',').Length;
            string[] parameter_id = new string[columns_size];

            for (int j = 0; j < columns_size; j++)
                parameter_id[j] = "";

			string[] line = contracts[0].Split(',');
            for (int i = 0; i < 7; i++)
            {
                string[] next_line = contracts[i + 1].Split(',');
                for (int j = 0; j < columns_size; j++)
                    if (!line[j].Equals("*"))
                        parameter_id[j] += line[j] + (!next_line[j].Equals("*") ? "-" : "");
                line = next_line;
            }

            for (int i = 8; i < contracts.Length; i++)
            {
				string[] arguments = contracts[i].Split(',');

                string family = arguments[0];
                string type = arguments[1];
                string size = arguments[2];

				ContextualContractType contract = new ContextualContractType();
                string name_concrete = "br.ufc.mdcc.backend.ec2.impl.platform." + family + ".EC2_" + type + "_" + size + "_impl"; 

                contract.name = "br.ufc.mdcc.backend.ec2.EC2Platform";

                IList<ContextArgumentType> context_argument_list = new List<ContextArgumentType>();

                for (int k = 0; k < arguments.Length; k++)
                {
                    string value = null;

                    switch (k)
                    {
                        case 0: value = "br.ufc.mdcc.backend.ec2.family." + family_name[family]; break;
                        case 1: value = "br.ufc.mdcc.backend.ec2.type." + family + "." + type; break;
                        case 2: value = "br.ufc.mdcc.backend.ec2.size." + size; break;
                        default: value = arguments[k]; break;
                    }

                    if (!value.Equals("*"))
                    {
                        ContextArgumentType context_argument = new ContextArgumentType();
                        context_argument_list.Add(context_argument);

                        context_argument.parameter_id = parameter_id[k];
                        context_argument.typeSpecified = true;
                        context_argument.type = decimal.TryParse(value, out _) ? ContextArgumentTypeType.quantifier : ContextArgumentTypeType.component_name;
                        context_argument.argument = value;

                        if (context_argument.type == ContextArgumentTypeType.component_name)
                        {
                            string c_name = value.Substring(value.LastIndexOf(".") + 1);

							string hpe_file_name = Path.Combine(CATALOG_PATH, value, c_name + ".hpe");
							string output_signature_file_name = Path.Combine(OUTPUT_PATH, framework_path, value + ".signature");
							mk_signature(hpe_file_name, output_signature_file_name);
						}
                    }
                }

                contract.context_argument = new ContextArgumentType[context_argument_list.Count];
                context_argument_list.CopyTo(contract.context_argument, 0);

                string output_file_name = Path.Combine(OUTPUT_PATH, framework_path, name_concrete + ".contract");
                writeContract(output_file_name, contract);
            }
        }

        private static void read_ec2_contracts_from_csv_2(string file_name, DoWithLine do_with_line)
		{
			string[] contracts = File.ReadAllLines(file_name);
			int columns_size = contracts[0].Split(',').Length;
			string[] parameter_id = new string[columns_size];

			for (int j = 0; j < columns_size; j++)
				parameter_id[j] = "";

			string[] line = contracts[0].Split(',');
			for (int i = 0; i < 7; i++)
			{
				string[] next_line = contracts[i + 1].Split(',');
				for (int j = 0; j < columns_size; j++)
					if (!line[j].Equals("*"))
						parameter_id[j] += line[j] + (!next_line[j].Equals("*") ? "-" : "");
				line = next_line;
			}

			for (int i = 8; i < contracts.Length; i++)
			{
				string[] arguments = contracts[i].Split(',');
				
                do_with_line(arguments, parameter_id);
			}
		}

        public delegate void DoWithLine(string[] arguments, string[] parameter_id);


        private static void new_ec2_abstractcomponent(string[] arguments, string[] parameter_id)
		{
            load_ec2_platform_parameters();

			string family = arguments[0];
			string type = arguments[1];
			string size = arguments[2];

            string name_abstract = "br.ufc.mdcc.backend.ec2.platform." + family + ".EC2_" + type + "_" + size;

            string file_name = Path.Combine(CATALOG_PATH, name_abstract, "EC2_" + type + "_" + size + ".hpe");

//            if (File.Exists(file_name + ".ok21"))
//                return;

//            if (File.Exists(file_name + ".ok20")) File.Delete(file_name + ".ok20");

			ComponentType c = null;
			give_parameters_and_supplies(file_name, out c, out _, out _, out _);
//			File.WriteAllText(file_name + ".ok21", serialize<ComponentType>(c));

            List<ParameterRenaming> renaming_list = new List<ParameterRenaming>();
            foreach (Tuple<ParameterType,InnerComponentType> par in ec2_platform_parameters.Values)
            {
                ParameterRenaming renaming_item = new ParameterRenaming();
                renaming_item.formFieldId = par.Item1.formFieldId;
                renaming_item.order = par.Item1.order;
                renaming_item.varName  = par.Item1.varName;
                renaming_list.Add(renaming_item);
            }

            renaming_list.Sort(delegate (ParameterRenaming x, ParameterRenaming y) { return x.order - y.order; });
            c.header.baseType.component.parameter = new ParameterRenaming[renaming_list.Count];
            renaming_list.CopyTo(c.header.baseType.component.parameter, 0);

			ParameterType parameter_count = (ParameterType)ec2_platform_parameters["node-count"].Item1.Clone();
            parameter_count.orderSpecified = true;
            parameter_count.order = 0;
			InnerComponentType ic_count = (InnerComponentType) ec2_platform_parameters["node-count"].Item2.Clone();
			
            ParameterType parameter_storage_size = (ParameterType)ec2_platform_parameters["node-storage-size"].Item1.Clone();
			parameter_storage_size.orderSpecified = true;
			parameter_storage_size.order = 1;
			InnerComponentType ic_storage_size = (InnerComponentType)ec2_platform_parameters["node-storage-size"].Item2.Clone();
			
            ParameterType parameter_cost = (ParameterType)ec2_platform_parameters["node-cost_per_hour"].Item1.Clone();
			parameter_cost.orderSpecified = true;
			parameter_cost.order = 2;
			InnerComponentType ic_cost = (InnerComponentType)ec2_platform_parameters["node-cost_per_hour"].Item2.Clone();

            ParameterType parameter_locale = (ParameterType)ec2_platform_parameters["node-locale"].Item1.Clone();
            parameter_locale.orderSpecified = true;
            parameter_locale.order = 3;
			InnerComponentType ic_locale = (InnerComponentType)ec2_platform_parameters["node-locale"].Item2.Clone();
            			
            IList<ParameterSupplyType> context_argument_list = new List<ParameterSupplyType>();
			IList<InnerComponentType> context_argument_ic_list = new List<InnerComponentType>();

            IDictionary<string, string> ignore = new Dictionary<string, string>();

          /*  IDictionary<string, string> family_name = new Dictionary<string, string>() 
            { 
                { "general", "GeneralPurpose" }, 
                { "compute", "Computation" }, 
                { "accelerated", "Acceleration" }, 
                { "memory", "Memory" }, 
                { "storage", "Storage" } 
            };*/

            ParameterSupplyType supply_family = new ParameterSupplyType();
            supply_family.cRef = "family";
            supply_family.direct = true;
            supply_family.varName = "F";

            InnerComponentType ic_family = new InnerComponentType();
            ic_family.localRef = "family";
            ic_family.location = Path.Combine("br.ufc.mdcc.backend.ec2.family." + family_name[family], family_name[family] + ".hpe");
            ic_family.multipleSpecified = true;
            ic_family.multiple = false;
            ic_family.name = family_name[family];
            ic_family.package = "br.ufc.mdcc.backend.ec2.family";
            ic_family.exposedSpecified = true;
            ic_family.exposed = false;

			ParameterSupplyType supply_type = new ParameterSupplyType();
			supply_type.cRef = "type";
			supply_type.direct = true;
			supply_type.varName = "T";

			InnerComponentType ic_type = new InnerComponentType();
			ic_type.localRef = "type";
            ic_type.location = Path.Combine("br.ufc.mdcc.backend.ec2.type." + family + "." + type, type + ".hpe");
			ic_type.multipleSpecified = true;
			ic_type.multiple = false;
            ic_type.name = type;
            ic_type.package = "br.ufc.mdcc.backend.ec2.type." + family;
			ic_type.exposedSpecified = true;
			ic_type.exposed = false;

			ParameterSupplyType supply_size = new ParameterSupplyType();
			supply_size.cRef = "size";
			supply_size.direct = true;
			supply_size.varName = "L";

			InnerComponentType ic_size = new InnerComponentType();
			ic_size.localRef = "size";
			ic_size.location = Path.Combine("br.ufc.mdcc.backend.ec2.size." + size, size + ".hpe");
			ic_size.multipleSpecified = true;
			ic_size.multiple = false;
			ic_size.name = size;
			ic_size.package = "br.ufc.mdcc.backend.ec2.size";
			ic_size.exposedSpecified = true;
			ic_size.exposed = false;

            for (int k = 3; k < arguments.Length; k++)
            {
                string value = arguments[k];

                if (ec2_platform_parameters.ContainsKey(parameter_id[k]) && !(new List<string>() { "node-count","node-storage-size","node-cost_per_hour","node-locale"}).Contains(parameter_id[k])) 
                {
                    //if (ec2_platform_parameters.ContainsKey(parameter_id[k]))
                    //{
                    //ignore[parameter_id[k]] = parameter_id[k];

                    ParameterSupplyType context_argument = new ParameterSupplyType();
                    context_argument_list.Add(context_argument);

                    context_argument.cRef = parameter_id[k];
                    context_argument.varName = ec2_platform_parameters[context_argument.cRef].Item1.varName;
                    context_argument.direct = true;

                    ContextArgumentTypeType argument_type = decimal.TryParse(value, out _) || value.Equals("#") ? ContextArgumentTypeType.quantifier : ContextArgumentTypeType.component_name;

                    InnerComponentType context_argument_ic = (InnerComponentType) ec2_platform_parameters[context_argument.cRef].Item2.Clone();

                    context_argument_ic_list.Add(context_argument_ic);

                    if (!value.Equals("*") && !value.Equals("#"))
                    {
                        if (argument_type == ContextArgumentTypeType.component_name)
                        {
                            context_argument_ic.package = value.Substring(0, value.LastIndexOf('.'));
                            context_argument_ic.name = value.Substring(value.LastIndexOf('.') + 1);
                            context_argument_ic.location = Path.Combine(context_argument_ic.package + "." + context_argument_ic.name, context_argument_ic.name + ".hpe");
                            context_argument_ic.unitBounds = null;

                            adjust_parameter_renamings(context_argument_ic);
                        }
                        else if (argument_type == ContextArgumentTypeType.quantifier)
                        {
                            context_argument_ic.quantifier_value = value;
                        }
                    }
                    else
                    {
                        Console.WriteLine(parameter_id[k] + " default ...");
                        if (argument_type == ContextArgumentTypeType.component_name)
                            adjust_parameter_renamings(context_argument_ic);
                    }
                }
            }

			IList<object> o_list = new List<object>();

            foreach (UnitType o in unit_list)
            {
                o.iRef = "IEC2_" + type + "_" + size;
                o.super = new UnitRefType[1];
                o.super[0] = new UnitRefType();
                o.super[0].cRef = "base";
                o.super[0].uRef = "node";
                o.super[0].slice_replica = 0;
                o_list.Add(o);
            }

            foreach (InterfaceType o in interface_list)
            {
                o.iRef = "IEC2_" + type + "_" + size;
                o.nArgs = 4;
                o.slice = new InterfaceSliceType[0];
                o.parameter = new InterfaceParameterType[4];

                o.parameter[0] = new InterfaceParameterType();
                o.parameter[0].iname = "IntUp";
                o.parameter[0].orderSpecified = true;
                o.parameter[0].order = 0;
                o.parameter[0].parid = "node-count";
                o.parameter[0].uname = "int_up";
                o.parameter[0].varid = "NCT";

                o.parameter[1] = new InterfaceParameterType();
                o.parameter[1].iname = "IntUp";
                o.parameter[1].orderSpecified = true;
                o.parameter[1].order = 1;
                o.parameter[1].parid = "node-storage-size";
                o.parameter[1].uname = "int_up";
                o.parameter[1].varid = "NOD_STO_SIZ";

                o.parameter[2] = new InterfaceParameterType();
                o.parameter[2].iname = "DecimalDown";
                o.parameter[2].orderSpecified = true;
                o.parameter[2].order = 2;
                o.parameter[2].parid = "node-cost_per_hour";
                o.parameter[2].uname = "decimal_down";
                o.parameter[2].varid = "CPH";

                o.parameter[3] = new InterfaceParameterType();
                o.parameter[3].iname = "IAnyWhere";
                o.parameter[3].orderSpecified = true;
                o.parameter[3].order = 3;
                o.parameter[3].parid = "node-locale";
                o.parameter[3].uname = "anywhere";
                o.parameter[3].varid = "LOC";

                o.sources[0].moduleName = name_abstract + "." + o.iRef;

                o.sources[0].file[0].name = "Base" + o.iRef + ".cs";
                string file_base_path = Path.Combine(CATALOG_PATH, name_abstract, "src", "1.0.0.0", o.sources[0].file[0].name);
                o.sources[0].file[0].contents = File.Exists(file_base_path) ? File.ReadAllText(file_base_path) : "";

                o.sources[0].file[1].name = o.iRef + ".cs";
                string file_user_path = Path.Combine(CATALOG_PATH, name_abstract, "src", "1.0.0.0", o.sources[0].file[1].name);
                o.sources[0].file[1].contents = File.Exists(file_user_path) ? File.ReadAllText(file_user_path) : "";

                o_list.Add(o);
            }

            o_list.Add(supply_family);
            o_list.Add(supply_type);
            o_list.Add(supply_size);
            o_list.Add(ic_family);
            o_list.Add(ic_type);
            o_list.Add(ic_size);
			o_list.Add(parameter_cost);
			o_list.Add(parameter_locale);
            o_list.Add(parameter_storage_size);
			o_list.Add(parameter_count);
            o_list.Add(ic_cost);
			o_list.Add(ic_locale);
            o_list.Add(ic_storage_size);
			o_list.Add(ic_count);
			foreach (ParameterSupplyType pst in context_argument_list) o_list.Add(pst);
			foreach (InnerComponentType pst in context_argument_ic_list) o_list.Add(pst);

			c.componentInfo = new object[o_list.Count];
			o_list.CopyTo(c.componentInfo, 0);

            File.WriteAllText(Path.Combine(CATALOG_PATH, name_abstract, "EC2_" + type + "_" + size + ".hpe"), serialize<ComponentType>(c));
		}

        private static void adjust_parameter_renamings(InnerComponentType context_argument_ic)
        {
			IDictionary<string, ParameterRenaming> renaming = new Dictionary<string, ParameterRenaming>();
			if (context_argument_ic.parameter != null)
				foreach (ParameterRenaming ipar in context_argument_ic.parameter)
					renaming[ipar.formFieldId] = ipar;

			IList<ParameterType> par_of_inner_list = new List<ParameterType>();
			ComponentType arg_c = deserialize<ComponentType>(File.ReadAllText(Path.Combine(CATALOG_PATH, context_argument_ic.location)));
			foreach (object o in arg_c.componentInfo)
				if (o is ParameterType)
					par_of_inner_list.Add((ParameterType)o);

			IList<ParameterRenaming> new_renaming = new List<ParameterRenaming>();
            foreach (ParameterType par_of_inner in par_of_inner_list)
            {
                if (renaming.ContainsKey(par_of_inner.formFieldId))
                    new_renaming.Add(renaming[par_of_inner.formFieldId]);
            }

			context_argument_ic.parameter = new ParameterRenaming[new_renaming.Count];
			new_renaming.CopyTo(context_argument_ic.parameter, 0);
		}

        private static IDictionary<string, Tuple<ParameterType, InnerComponentType>> ec2_platform_parameters = null;
        private static IList<UnitType> unit_list = new List<UnitType>();
        private static IList<InterfaceType> interface_list = new List<InterfaceType>();

        private static void load_ec2_platform_parameters()
        {
            if (ec2_platform_parameters != null)
                return;

            ec2_platform_parameters = new Dictionary<string, Tuple<ParameterType, InnerComponentType>>();

            ComponentType c = deserialize<ComponentType>(File.ReadAllText(Path.Combine(CATALOG_PATH, "br.ufc.mdcc.backend.ec2.EC2Platform", "EC2Platform.hpe")));

            for (int k = 0; k < c.componentInfo.Length; k++)
            {
                object o = c.componentInfo[k];
                if (o is ParameterType)
                {
                    ParameterType p = (ParameterType)o;
                    if (ec2_platform_parameters.ContainsKey(p.formFieldId) && ec2_platform_parameters[p.formFieldId].Item1 == null)
                        ec2_platform_parameters[p.formFieldId] = new Tuple<ParameterType, InnerComponentType>(p, ec2_platform_parameters[p.formFieldId].Item2);
                    else if (!ec2_platform_parameters.ContainsKey(p.formFieldId))
                        ec2_platform_parameters[p.formFieldId] = new Tuple<ParameterType, InnerComponentType>(p, null);
                }
                else if (o is InnerComponentType)
                {
                    InnerComponentType ic = (InnerComponentType)o;
                    if (ec2_platform_parameters.ContainsKey(ic.localRef) && ec2_platform_parameters[ic.localRef].Item2 == null)
                        ec2_platform_parameters[ic.localRef] = new Tuple<ParameterType, InnerComponentType>(ec2_platform_parameters[ic.localRef].Item1, ic);
                    else if (!ec2_platform_parameters.ContainsKey(ic.localRef))
                        ec2_platform_parameters[ic.localRef] = new Tuple<ParameterType, InnerComponentType>(null, ic);
                }
                else if (o is UnitType)
                {
                    unit_list.Add((UnitType)o);
                }
                else if (o is InterfaceType)
                {
                    interface_list.Add((InterfaceType)o);
                }
            }

            IList<string> g_list = new List<string>();

            foreach (KeyValuePair<string, Tuple<ParameterType, InnerComponentType>> parameter in ec2_platform_parameters)
                if (parameter.Value.Item1 == null)
                    g_list.Add(parameter.Key);

            foreach (string g in g_list)
                ec2_platform_parameters.Remove(g);
		}

        private static void new_ec2_contract(string[] arguments, string[] parameter_id)
        {
			string family = arguments[0];
			string type = arguments[1];
			string size = arguments[2];

            string name_abstract = "br.ufc.mdcc.backend.ec2.platform." + family + ".EC2_" + type + "_" + size;
			string name_concrete = "br.ufc.mdcc.backend.ec2.impl.platform." + family + ".EC2_" + type + "_" + size;

			AbstractComponentSignatureType signature = new AbstractComponentSignatureType();
			signature.name = name_abstract;
			signature.kind = AbstractComponentSignatureTypeKind.platform;
			signature.kindSpecified = true;

			signature.context_parameter = new ContextParameterType[4];
			signature.context_parameter[0] = new ContextParameterType();
   			signature.context_parameter[0].parameter_id = "node-count";
			signature.context_parameter[0].parameter_typeSpecified = true;
            signature.context_parameter[0].parameter_type = ContextParameterTypeParameter_type.platform;
			signature.context_parameter[0].bound = new ContextualContractType();
			signature.context_parameter[0].bound.name = "1#i+";
			signature.context_parameter[0].bound.is_quantifier = true;

			signature.context_parameter[1] = new ContextParameterType();
			signature.context_parameter[1].parameter_id = "node-storage-size";
			signature.context_parameter[1].parameter_typeSpecified = true;
            signature.context_parameter[1].parameter_type = ContextParameterTypeParameter_type.platform;
			signature.context_parameter[1].bound = new ContextualContractType();
			signature.context_parameter[1].bound.name = "20f#i+";
			signature.context_parameter[1].bound.is_quantifier = true;

			signature.context_parameter[2] = new ContextParameterType();
			signature.context_parameter[2].parameter_id = "node-cost_per_hour";
			signature.context_parameter[2].parameter_typeSpecified = true;
			signature.context_parameter[2].parameter_type = ContextParameterTypeParameter_type.cost;
			signature.context_parameter[2].bound = new ContextualContractType();
			signature.context_parameter[2].bound.name = "+inf#i-";
			signature.context_parameter[2].bound.is_quantifier = true;

			signature.context_parameter[3] = new ContextParameterType();
			signature.context_parameter[3].parameter_id = "node-locale";
			signature.context_parameter[3].parameter_typeSpecified = true;
			signature.context_parameter[3].parameter_type = ContextParameterTypeParameter_type.platform;
			signature.context_parameter[3].bound = new ContextualContractType();
			signature.context_parameter[3].bound.name = "br.ufc.mdcc.hpcshelf.platform.locale.AnyWhere";
			signature.context_parameter[3].bound.is_quantifier = false;

			ContextualContractType contract_super = signature.super_type = new ContextualContractType();

			contract_super.name = "br.ufc.mdcc.backend.ec2.EC2Platform";
			string hpe_file_name = Path.Combine(CATALOG_PATH, contract_super.name, "EC2Platform.hpe");
			string output_signature_file_name = Path.Combine(OUTPUT_PATH, framework_path, contract_super.name + ".signature");
			mk_signature(hpe_file_name, output_signature_file_name);

			IList<ContextArgumentType> context_argument_list = new List<ContextArgumentType>();

			for (int k = 0; k < arguments.Length; k++)
			{
				string value = null;

				switch (k)
				{
					case 0: value = "br.ufc.mdcc.backend.ec2.family." + family_name[family]; break;
					case 1: value = "br.ufc.mdcc.backend.ec2.type." + family + "." + type; break;
					case 2: value = "br.ufc.mdcc.backend.ec2.size." + size; break;
					default: value = arguments[k]; break;
				}

				if (!value.Equals("*"))
				{
					ContextArgumentType context_argument = new ContextArgumentType();
					context_argument_list.Add(context_argument);

					context_argument.parameter_id = parameter_id[k];
					context_argument.typeSpecified = true;
					context_argument.type = decimal.TryParse(value, out _) ? ContextArgumentTypeType.quantifier : ContextArgumentTypeType.component_name;
					context_argument.argument = value;

					if (context_argument.type == ContextArgumentTypeType.component_name)
					{
						string c_name = value.Substring(value.LastIndexOf(".") + 1);

						string hpe_file_name_arg = Path.Combine(CATALOG_PATH, value, c_name + ".hpe");
						string output_signature_file_name_arg = Path.Combine(OUTPUT_PATH, framework_path, value + ".signature");
						mk_signature(hpe_file_name_arg, output_signature_file_name_arg);
					}
				}
			}

			ContextArgumentType context_argument_nodecount = new ContextArgumentType();
			context_argument_list.Add(context_argument_nodecount);
			context_argument_nodecount.parameter_id = "node-count";
			context_argument_nodecount.typeSpecified = true;
			context_argument_nodecount.type = ContextArgumentTypeType.outer_parameter;
			context_argument_nodecount.argument = "node-count";

			ContextArgumentType context_argument_nodestoragesize = new ContextArgumentType();
			context_argument_list.Add(context_argument_nodestoragesize);
			context_argument_nodestoragesize.parameter_id = "node-storage-size";
			context_argument_nodestoragesize.typeSpecified = true;
			context_argument_nodestoragesize.type = ContextArgumentTypeType.outer_parameter;
			context_argument_nodestoragesize.argument = "node-storage-size";
			
            ContextArgumentType context_argument_cost = new ContextArgumentType();
			context_argument_list.Add(context_argument_cost);
			context_argument_cost.parameter_id = "node-cost_per_hour";
			context_argument_cost.typeSpecified = true;
			context_argument_cost.type = ContextArgumentTypeType.outer_parameter;
			context_argument_cost.argument = "node-cost_per_hour";

			ContextArgumentType context_argument_locale = new ContextArgumentType();
			context_argument_list.Add(context_argument_locale);
			context_argument_locale.parameter_id = "node-locale";
			context_argument_locale.typeSpecified = true;
			context_argument_locale.type = ContextArgumentTypeType.outer_parameter;
			context_argument_locale.argument = "node-locale";

			contract_super.context_argument = new ContextArgumentType[context_argument_list.Count];
			context_argument_list.CopyTo(contract_super.context_argument, 0);

			ContextualContractType contract = new ContextualContractType();
			contract.name = name_abstract;

			string signature_file_name = Path.Combine(OUTPUT_PATH, framework_path, name_abstract + ".signature");
			writeSignature(signature_file_name, signature);

			string contract_file_name = Path.Combine(OUTPUT_PATH, "ec2_base_contracts", name_concrete + ".contract");
			writeContract(contract_file_name, contract);
		}

        private static void mk_contract(string path_in, string path_out)
        {
			ComponentType c;
			
            readInput(path_in, out c);
            ContextualContractType contract = mk_contract(c);
  		    writeContract(path_out, contract);
		}

        private static ContextualContractType mk_contract(ComponentType c)
        {
			List<Tuple<int, ParameterType>> parameter_list = new List<Tuple<int, ParameterType>>();
			IDictionary<string, InnerComponentType> inner_component_dict = new Dictionary<string, InnerComponentType>();
			IDictionary<string, ParameterType> parameter_by_var_dict = new Dictionary<string, ParameterType>();
			IDictionary<string, ParameterSupplyType> supply_by_var_dict = new Dictionary<string, ParameterSupplyType>();

			for (int k = 0; k < c.componentInfo.Length; k++)
			{
				object o = c.componentInfo[k];
				if (o is ParameterType)
				{
					ParameterType p = (ParameterType)o;
					parameter_list.Add(new Tuple<int, ParameterType>(p.order, p));
					parameter_by_var_dict[p.varName] = p;
				}
				else if (o is InnerComponentType)
				{
					InnerComponentType ic = (InnerComponentType)o;
					inner_component_dict[ic.localRef] = ic;
				}
				else if (o is ParameterSupplyType)
				{
					ParameterSupplyType p = (ParameterSupplyType)o;
					supply_by_var_dict[p.varName] = p;
				}
			}

			parameter_list.Sort(new Sorter<ParameterType>());

			return mk_contract(c.header.baseType.component, parameter_by_var_dict, supply_by_var_dict, inner_component_dict);
        }

        class Sorter<T> : IComparer<Tuple<int,T>>
		{
			public int Compare(Tuple<int,T> x, Tuple<int,T> y)
			{
                return x.Item1.CompareTo(y.Item1);

			}
		}

        private static IDictionary<VarianceType, ContextParameterTypeVariance> variance_mapping =
            new Dictionary<VarianceType, ContextParameterTypeVariance>()
                        {
                            {VarianceType.contravariant,ContextParameterTypeVariance.contravariant},
                            {VarianceType.covariant,ContextParameterTypeVariance.covariant},
                            {VarianceType.invariant,ContextParameterTypeVariance.invariant}
                        };

		private static AbstractComponentSignatureType mk_signature(ComponentType c)
        {
            AbstractComponentSignatureType signature = new AbstractComponentSignatureType();
            signature.name = c.header.packagePath + "." + c.header.name;
            signature.kindSpecified = true;
            if (c.header.kind == SupportedKinds.Platform)
                signature.kind = AbstractComponentSignatureTypeKind.platform;
            else
                signature.kind = AbstractComponentSignatureTypeKind.computation;

            if (c.header.baseType != null)
            {
                ComponentInUseType super = c.header.baseType.component;

                string hpe_file_name = Path.Combine(CATALOG_PATH, super.package + "." + super.name, super.name + ".hpe");
				string output_file_name = Path.Combine(OUTPUT_PATH, framework_path, super.package + "." + super.name + ".signature");

				mk_signature(hpe_file_name, output_file_name);
            }

            List<Tuple<int, ParameterType>> parameter_list = new List<Tuple<int, ParameterType>>();
            IDictionary<string,InnerComponentType> inner_component_dict = new Dictionary<string, InnerComponentType>();
            IDictionary<string, ParameterType> parameter_by_var_dict = new Dictionary<string, ParameterType>();
			IDictionary<string, ParameterSupplyType> supply_by_var_dict = new Dictionary<string, ParameterSupplyType>();

			for (int k = 0; k < c.componentInfo.Length; k++)
            {
                object o = c.componentInfo[k];
                if (o is ParameterType)
                {
                    ParameterType p = (ParameterType)o;
                    parameter_list.Add(new Tuple<int, ParameterType>(p.order, p));
                    parameter_by_var_dict[p.varName] = p;
                }
                else if (o is InnerComponentType)
                {
                    InnerComponentType ic = (InnerComponentType)o;
                    inner_component_dict[ic.localRef] = ic;
                }
                else if (o is ParameterSupplyType)
                {
                    ParameterSupplyType p = (ParameterSupplyType)o;
                    supply_by_var_dict[p.varName] = p;
                }

            }

            parameter_list.Sort(new Sorter<ParameterType>());

            List<ContextParameterType> parameter_out_list = new List<ContextParameterType>();

            if (c.header.baseType != null)
			    signature.super_type = mk_contract(c.header.baseType.component, parameter_by_var_dict, supply_by_var_dict, inner_component_dict);

			foreach (Tuple<int,ParameterType> parameter in parameter_list)
            {
                ContextParameterType parameter_out = new ContextParameterType();
                parameter_out_list.Add(parameter_out);

                string parameter_id = parameter.Item2.formFieldId;
                string cRef = parameter.Item2.componentRef;
                VarianceType variance = parameter.Item2.variance;
                string varName = parameter.Item2.varName;
                InnerComponentType ic = inner_component_dict[cRef];

                parameter_out.parameter_id = parameter_id;
                parameter_out.parameter_type = ContextParameterTypeParameter_type.platform;
                parameter_out.variance = variance_mapping[variance];
                parameter_out.bound = mk_contract(ic, parameter_by_var_dict, supply_by_var_dict, inner_component_dict);

				string hpe_file_name = Path.Combine(CATALOG_PATH, ic.package + "." + ic.name, ic.name + ".hpe");
				string output_file_name = Path.Combine(OUTPUT_PATH, framework_path, ic.package + "." + ic.name + ".signature");
				
                mk_signature(hpe_file_name, output_file_name);
            }

            signature.context_parameter = new ContextParameterType[parameter_out_list.Count];
            parameter_out_list.CopyTo(signature.context_parameter, 0);

            return signature;
        }

        private static IDictionary<string, string> quantifier_prefix = new Dictionary<string, string>() 
                                                                { { "IntDown", "i-" }, 
                                                                  { "IntUp", "i+" }, 
                                                                  { "DecimalDown", "d-" }, 
                                                                  { "DecimalUp", "d+" } };

        private static ContextualContractType mk_contract(ComponentInUseType c, 
                                                          IDictionary<string, ParameterType> parameter_by_var_dict,
                                                          IDictionary<string, ParameterSupplyType> supply_by_var_dict,
                                                          IDictionary<string, InnerComponentType> inner_component_dict)
        {
            ContextualContractType contract = new ContextualContractType();
            if (c.quantifier_value != null)
            {
                contract.name = c.quantifier_value + "#" + quantifier_prefix[c.name]; 
                contract.is_quantifier = true;
            }
            else
            {
                contract.name = c.package + "." + c.name;
                contract.is_quantifier = false;

				string hpe_file_name_base = Path.Combine(CATALOG_PATH, c.package + "." + c.name, c.name + ".hpe");
				string output_file_name_base = Path.Combine(OUTPUT_PATH, framework_path, c.package + "." + c.name + ".signature");
				mk_signature(hpe_file_name_base, output_file_name_base);

				IList<ContextArgumentType> arguments_list = new List<ContextArgumentType>();

                if (c.parameter != null)
	                foreach (ParameterRenaming p in c.parameter)
	                {
	                    ContextArgumentType context_argument = new ContextArgumentType();
                        arguments_list.Add(context_argument);
	                    context_argument.parameter_id = p.formFieldId;
	                    context_argument.typeSpecified = true;

						ParameterType parameter = null;
                        ParameterSupplyType supply = null;
                        if (supply_by_var_dict.TryGetValue(p.varName, out supply))
                        {
                            InnerComponentType ic_par = inner_component_dict[supply.cRef];
	                        if (ic_par.quantifier_value != null)
	                        {
	                            context_argument.type = ContextArgumentTypeType.quantifier;
                                context_argument.typeSpecified = true;
	                            context_argument.argument = ic_par.quantifier_value + "#" + quantifier_prefix[ic_par.name]; 
	                        }
	                        else
	                        {
	                            context_argument.type = ContextArgumentTypeType.component_name;
                                context_argument.typeSpecified = true;
	                            context_argument.argument = ic_par.package + "." + ic_par.name;

								string hpe_file_name = Path.Combine(CATALOG_PATH, ic_par.package + "." + ic_par.name, ic_par.name + ".hpe");
								string output_file_name = Path.Combine(OUTPUT_PATH, framework_path, ic_par.package + "." + ic_par.name + ".signature");
								mk_signature(hpe_file_name, output_file_name);
	                        }
	                    }
						else if (parameter_by_var_dict.TryGetValue(p.varName, out parameter))
                        {
							InnerComponentType ic_par = inner_component_dict[parameter.componentRef];
	                        context_argument.type = ContextArgumentTypeType.outer_parameter;
                            context_argument.typeSpecified = true;
	                        context_argument.argument = parameter_by_var_dict[p.varName].formFieldId;
	                    }
	                }

                contract.context_argument = new ContextArgumentType[arguments_list.Count];
                arguments_list.CopyTo(contract.context_argument, 0);
            }

            return contract;
        }

        private static void readInput(string file_name, out ComponentType c)
		{
			string module_data = File.ReadAllText(file_name);
			c = deserialize<ComponentType>(module_data);
		}


        private static void writeContract(string path, ContextualContractType contract)
		{
            string module_data_out = serialize<ContextualContractType>(contract);

			File.WriteAllText(path, module_data_out);
		}

        private static void writeSignature(string path, AbstractComponentSignatureType signature)
		{
			string module_data_out = serialize<AbstractComponentSignatureType>(signature);
			File.WriteAllText(path, module_data_out);
		}
		
        public static T deserialize<T>(string contents)
		{
			string filename = Path.GetTempFileName();
			File.WriteAllText(filename, contents);

			// Declare an object variable of the type to be deserialized.
			T i = default(T);
			FileStream fs = null;
			try
			{
				Console.WriteLine("Reading with XmlReader " + filename);

				// Create an instance of the XmlSerializer specifying type and namespace.
				XmlSerializer serializer = new XmlSerializer(typeof(T));

				// A FileStream is needed to read the XML document.
				fs = new FileStream(filename, FileMode.Open);

				XmlReader reader = new XmlTextReader(fs);

				// Use the Deserialize method to restore the object's state.
				i = (T)serializer.Deserialize(reader);

			}
			catch (Exception e)
			{
				Console.WriteLine(e.StackTrace);
			}
			finally
			{
				if (fs != null)
					fs.Close();
			}

			return i;

		}

		public static string serialize<T>(T instance)
		{
			XmlSerializer serializer = new XmlSerializer(typeof(T));

			string filename = Path.GetTempFileName();

			FileStream fs = new FileStream(filename, FileMode.Open);

			XmlWriter writer = new XmlTextWriter(fs, null);

			serializer.Serialize(writer, instance);

			fs.Close();

			return File.ReadAllText(filename);

		}


        private static IList<string> cs2 = new List<string>()
        {
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_T_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_T_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_T_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_T_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_T_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_T_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_T_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_T_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_T_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_T_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_N_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_N_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_N_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_N_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_N_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_N_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_N_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_N_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_N_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_N_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_C_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_C_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_C_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_C_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_C_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_C_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_C_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_C_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_C_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_U_C_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_T_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_T_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_T_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_T_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_T_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_T_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_T_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_T_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_T_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_T_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_N_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_N_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_N_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_N_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_N_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_N_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_N_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_N_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_N_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_N_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_C_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_C_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_C_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_C_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_C_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_C_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_C_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_C_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_C_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_U_L_C_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_T_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_T_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_T_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_T_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_T_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_T_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_T_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_T_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_T_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_T_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_N_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_N_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_N_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_N_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_N_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_N_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_N_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_N_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_N_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_N_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_C_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_C_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_C_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_C_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_C_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_C_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_C_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_C_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_C_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_U_C_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_T_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_T_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_T_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_T_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_T_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_T_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_T_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_T_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_T_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_T_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_N_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_N_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_N_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_N_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_N_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_N_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_N_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_N_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_N_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_N_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_C_R_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_C_R_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_C_R_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_C_R_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_C_R_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_C_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_C_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_C_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_C_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRSM_N_L_C_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_T_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_N_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_U_C_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_T_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_N_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_U_L_C_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_T_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_N_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_U_C_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_T_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_N_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRMM_N_L_C_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRAN_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRAN_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRAN_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRAN_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.TRAN_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_U_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYRK_L_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_U_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYR2K_L_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_U_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.SYMM_L_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_U_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HERK_L_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_U_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HER2K_L_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_U_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_R_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.HEMM_L_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_T_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_N_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_T_C_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_T_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_N_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_N_C_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_T_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_N_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level3.GEMM_C_C_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_U_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_U_L_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_U_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_T_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_T_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_T_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_T_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_T_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_N_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_N_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_N_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_N_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_N_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_C_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_C_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_C_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_C_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRSV_N_L_C_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_T_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_N_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_U_C_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_T_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_N_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_U_L_C_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_T_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_N_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_U_C_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_T_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_N_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.TRMV_N_L_C_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR2_U_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR2_U_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR2_U_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR2_U_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR2_U_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR2_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR2_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR2_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR2_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR2_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR_U_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR_U_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR_U_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR_U_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR_U_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYR_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_U_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.SYMV_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER_U_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER_U_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER_U_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER_U_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER_U_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER2_U_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER2_U_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER2_U_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER2_U_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER2_U_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER2_L_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER2_L_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER2_L_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER2_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER2_L_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HER_L_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_U_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.HEMV_L_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GER_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GER_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GER_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GER_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GER_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_T_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_N_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level2.GEMV_C_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.SWAP_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.SWAP_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.SWAP_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.SWAP_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.SWAP_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.SCAL_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.SCAL_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.SCAL_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.SCAL_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.SCAL_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.NRM2_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.NRM2_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.NRM2_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.NRM2_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.NRM2_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_SFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_SFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_SFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_SComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_SComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_SComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_Integer_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_Integer_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_Integer_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_DComplex_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_DComplex_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_DComplex_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_DFloat_Turing_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_DFloat_Kepler_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.DOT_DFloat_Architecture_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.COPY_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.COPY_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.COPY_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.COPY_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.COPY_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.AXPY_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.AXPY_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.AXPY_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.AXPY_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.AXPY_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.ASUM_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.ASUM_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.ASUM_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.ASUM_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.ASUM_DComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.AMAX_SFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.AMAX_SComplex_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.AMAX_Integer_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.AMAX_DFloat_",
            "br.ufc.mdcc.hpcshelf.blas.impl.level1.AMAX_DComplex_"
        };


        private static IList<string> cs1 = new List<string>()
        {
            "br.ufc.mdcc.hpcshelf.blas.level1.AMAX",
            "br.ufc.mdcc.hpcshelf.blas.level1.ASUM",
            "br.ufc.mdcc.hpcshelf.blas.level1.AXPY",
            "br.ufc.mdcc.hpcshelf.blas.level1.COPY",
            "br.ufc.mdcc.hpcshelf.blas.level1.DOT",
            "br.ufc.mdcc.hpcshelf.blas.level1.NRM2",
            "br.ufc.mdcc.hpcshelf.blas.level1.SCAL",
            "br.ufc.mdcc.hpcshelf.blas.level1.SWAP",
            "br.ufc.mdcc.hpcshelf.blas.level2.MV",
            "br.ufc.mdcc.hpcshelf.blas.level2.Rank1",
            "br.ufc.mdcc.hpcshelf.blas.level2.Rank2",
            "br.ufc.mdcc.hpcshelf.blas.level2.GEMV",
            "br.ufc.mdcc.hpcshelf.blas.level2.GER",
            "br.ufc.mdcc.hpcshelf.blas.level2.HEMV",
            "br.ufc.mdcc.hpcshelf.blas.level2.HER",
            "br.ufc.mdcc.hpcshelf.blas.level2.HER2",
            "br.ufc.mdcc.hpcshelf.blas.level2.SYMV",
            "br.ufc.mdcc.hpcshelf.blas.level2.SYR",
            "br.ufc.mdcc.hpcshelf.blas.level2.SYR2",
            "br.ufc.mdcc.hpcshelf.blas.level2.TRMV",
            "br.ufc.mdcc.hpcshelf.blas.level2.TRSV",
            "br.ufc.mdcc.hpcshelf.blas.level3.MM",
            "br.ufc.mdcc.hpcshelf.blas.level3.Rank1K",
            "br.ufc.mdcc.hpcshelf.blas.level3.Rank2K",
            "br.ufc.mdcc.hpcshelf.blas.level3.GEMM",
            "br.ufc.mdcc.hpcshelf.blas.level3.HEMM",
            "br.ufc.mdcc.hpcshelf.blas.level3.HER2K",
            "br.ufc.mdcc.hpcshelf.blas.level3.HERK",
            "br.ufc.mdcc.hpcshelf.blas.level3.SYMM",
            "br.ufc.mdcc.hpcshelf.blas.level3.SYR2K",
            "br.ufc.mdcc.hpcshelf.blas.level3.SYRK",
            "br.ufc.mdcc.hpcshelf.blas.level3.TRAN",
            "br.ufc.mdcc.hpcshelf.blas.level3.TRMM",
            "br.ufc.mdcc.hpcshelf.blas.level3.TRSM"
        };

    }
}
